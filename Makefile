LIBRARY_NAME = libirc

# All of the C files we'll need go here, but with a .o extension
OBJS = 

LIBS = 

CC = gcc
CFLAGS = -fPIC -ggdb3 -Wall -Werror -Wextra -D_GNU_SOURCE
LDFLAGS = -fPIC

all: #${LIBRARY_NAME}.so ${LIBRARY_NAME}.a

${LIBRARY_NAME}.so: ${OBJS}
	@echo "Building $@"
	@${CC} -shared -nostartfiles -nostdlib ${OBJS} ${LDFLAGS} -o $@

${LIBRARY_NAME}.a: ${OBJS}
	@echo "Building $@"
	@$(AR) rc ${LIBRARY_NAME}.a ${OBJS}
	@[ -z "$(RANLIB)" ] || $(RANLIB) ${LIBRARY_NAME}.a

%.o: %.c Makefile global.h
	@echo "Compiling $<"
	@${CC} ${CFLAGS} -c $< -o $@

%.i: %.c Makefile global.h
	@echo "Prepocessing $<"
	@${CC} ${CFLAGS} -E $< -o $@

clean:
	@echo "Cleaning ${LIBRARY_NAME}"
	@rm -f ${LIBRARY_NAME}.so ${LIBRARY_NAME}.a ${OBJS}

.PHONY: all clean
